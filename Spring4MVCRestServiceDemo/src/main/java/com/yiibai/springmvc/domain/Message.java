package com.yiibai.springmvc.domain;

/**
public class Message {

	String name;
	String text;

	public Message(String name, String text) {
		this.name = name;
		this.text = text;
	}

	public String getName() {
		return name;
	}

	public String getText() {
		return text;
	}

}*/

// 鏀寔XML杈撳嚭鐨�
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
//Spring4 MVC REST服务使用@RestController实例
//文章地址：https://www.yiibai.com/spring_mvc/spring-4-mvc-rest-service-example-using-restcontroller.html
@XmlRootElement(name = "pizza")
public class Message {

	String name;
	String text;

	public Message(){
		
	}
	
	public Message(String name, String text) {
		this.name = name;
		this.text = text;
	}

	@XmlElement
	public String getName() {
		return name;
	}
	
	@XmlElement
	public String getText() {
		return text;
	}

}

package com.svlada;

//import io.jsonwebtoken.Jwt;

import com.auth0.jwt.interfaces.Claim;
import com.svlada.jwt.demo.JwtUtil;
import io.jsonwebtoken.Jwt;

import java.lang.reflect.Array;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
//        Integer[] numbers= {12,34,56,78,90};
//        numbers.stream().filter(i -> i % 2 == 0).distinct().forEach(System.out::println);
        //lambda 写法
//        new Thread(() -> System.out.println("lambda写法")).start();

//获取token
       /* Map<String , Object> payload=new HashMap<String, Object>();
        Date date=new Date();
        payload.put("uid", "291969452");//用户id
        payload.put("iat", date.getTime());//生成时间
        payload.put("ext",date.getTime()+1000*60*60);//过期时间1小时
        String token= Jwt.createToken(payload);
        System.out.println("token:"+token);*/

        try {
            String token = JwtUtil.createToken("12345", "wangbo");
            System.out.println("token=" + token);
            //Thread.sleep(5000);
            Map<String, Claim> map = JwtUtil.verifyToken(token);
            //Map<String, Claim> map = JwtUtil.parseToken(token);
            //遍历
            for (Map.Entry<String, Claim> entry : map.entrySet()){
                if (entry.getValue().asString() != null){
                    System.out.println(entry.getKey() + "===" + entry.getValue().asString());
                }else {
                    System.out.println(entry.getKey() + "===" + entry.getValue().asDate());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiLmtYvor5UiLCJ1c2VyTmFtZSI6IndhbmdibyIsImV4cCI6MTU3NDY3OTE5MiwidXNlcklkIjoiMTIzNDUiLCJpYXQiOjE1NzQ2NzkxNzd9.BXfZrDRb2xUnjXP-PCOijI7zobOvU09-dZhkaVosVSw";
        try {
            Map<String,Claim> rs = JwtUtil.verifyToken(token);
            System.out.println("返回结果数据是：" +rs.toString());
            System.out.println("返回结果数据是：" +rs.get("state"));
            for (Map.Entry<String, Claim> entry : rs.entrySet()){
                if (entry.getValue().asString() != null){
                    System.out.println(entry.getKey() + "===" + entry.getValue().asString());
                }else {
                    System.out.println(entry.getKey() + "===" + entry.getValue().asDate());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

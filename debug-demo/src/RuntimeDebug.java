/**
 * 
 */

/**
 * @author admin  varialbes变量视图  运行时动态修改变量值
 * 
 * url : https://blog.csdn.net/u011277123/article/details/64906091
 *
 * idea : 1. 可以在debug断点过程中直接右键setValue修改参数的值；
 */
public class RuntimeDebug {

	/**
	 * 
	 */
	public RuntimeDebug() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a = 1;
		int b = 2;
		int c = a + b;
		System.out.println(c);
	}

}
